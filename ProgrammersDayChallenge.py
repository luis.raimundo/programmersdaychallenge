import random

def programmersDayChallenge():
    print("*******************************************")
    print("Welcome to the Programmer`s Day Challenge!")
    print("Select the right box for win a Apple Watch!")
    print("*******************************************")

    win = 0

    while(not win):
        secret_box = random.randrange(1, 50)

        print("Select 1 of the 50 box: [1][2][3]...[50]")
        choice_str = input()

        print("You chose ", choice_str)
        choice = int(choice_str)

        win = choice == secret_box

        if (choice < 1 or choice > 50):
            print("You must select between 1 and 50")
        if (win):
            print("Congrats! You won a apple watch!!!")
        else:
            print("Sorry! Try again! The secret box was {}.".format(secret_box))


print("End of game")

if (__name__ == "__main__"):
    programmersDayChallenge()
